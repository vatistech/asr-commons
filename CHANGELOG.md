## 2.1

### Features
* models cache store
* S3 object modeling
* Models config domain

### Fixes

## 2.0

### Features
* Multi-channels request option
* Spoken commands configuration options
* Find Replace configuration options
* REPLACED entity
* refactored entity group id type: int -> str
* Added flag to create final frames using spoken commands instead of silence detection
* Added speaker id on Word

### Fixes
* Model string representation


## 1.4

### Features
* added `ProcessedTranscriptionPacket`
* added processed transcript on `TranscriptionResponse`
* word recognized entity
* `LOCKED` stream status 
* `TOKENIZER` model type
* Speakers diarization configuration

### Fixes
* disabled post-processing by default

## 1.3

### Features
* `file_duration` on `Transcriptionrequest`
* `execution_time` profiling wrapper
* live stream events
* added string representation for stream packets
* add punctuation-capitalization and entity-recognition model types

### Fixes
* missing requirements
* `TranscriptionException` deserialization
* `TranscriptionRequest` protected attributes
* Removed type hint in enums

### Breaking changes
* Modified `BIT_16` value from `2` to `16` in `BitDepth`

## 1.2

### Features
* New `en_GB` language and default model
* Added configurable logging
* Added TranscriptionRequest, TranscriptionResponse and TranscriptionException models

### Fixes
* Default None speaker on Word
* Fixed missing numpy dependency

## 1.1
* Support for custom models
* Romanian MEDIA default model

