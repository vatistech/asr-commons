## Pre-requirements

* for each new tag, document the important changes in `CHANGELOG.md` (e.g. refactors, bug fixes, considerable improvements) and add required compatibility versions of other services if any 

## Steps

1. `cd` into the root folder and make sure the virtual environment si active
2. Install `twine`: `pip install twine`
3. Install `semantic_version`: `pip install semantic_version`

## Manual deploy and tagging
1. On the current commit add a tag that matches the `__tag__` in the `setup.py` and push the tag 
2. Empty the `./dist` directory
3. Create distribution: `python setup.py sdist`
4. Upload the distribution to PyPi: `python -m twine upload dist/*` (provide the username and password of the PyPi account)
5. Update `__tag__` in the `setup.py` for the next version, commit and push the change

## Using release script
1. Empty the `./dist` directory: `rm -r ./dist/*`
2. Create distribution: `python setup.py sdist`
3. Upload the distribution to PyPi: `python -m twine upload dist/*` (provide the username and password of the PyPi account)
4. Create release: `python ./release.py patch` (usage: `release [-h] [--git GIT] [--bash BASH] [--setup SETUP] {major,minor,patch}`)

## Build dependencies wheels

Run `python setup.py package`. This will create a folder `wheelhouse` containing all the dependencies needed.
Run `python setup.py bdist_wheel` to create the library wheel