# Hotwords headers ####################################################################################################
HOTWORDS_KEY = 'hotwords'
HOTWORDS_WEIGHT_KEY = 'weight'

# Post-processing headers #############################################################################################
DISABLE_DISFLUENCIES_HEADER: str = 'DisableDisfluencies'
ENABLE_PUNCTUATION_AND_CAPITALIZATION_HEADER: str = 'EnablePunctuationCapitalization'
ENABLE_ENTITIES_RECOGNITION_HEADER: str = 'EnableEntitiesRecognition'
ENABLE_NUMERALS_CONVERSION_HEADER: str = 'EnableNumeralsConversion'
SPEAKERS_DIARIZATION_HEADER: str = 'EnableSpeakersDiarization'
SPEAKERS_NUMBER_HEADER: str = 'SpeakersNumber'
ENABLE_MULTI_CHANNELS: str = 'EnableMultiChannels'
