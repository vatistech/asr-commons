import re
import shlex
import subprocess
import sys

import argparse

from semantic_version import Version

BASH: str = '/bin/bash'
GIT: str = '/usr/bin/git'
SETUP: str = 'setup.py'

CHECK_GIT_TREE_CLEAN_CMD: str = f'{BASH} -c \'if test -z "$({GIT} status --porcelain)"; then exit 0; else exit 1; fi\''


def increase_version(version, part):
    if part == 'major':
        return str(Version(version).next_major())
    elif part == 'minor':
        return str(Version(version).next_minor())
    elif part == 'patch':
        return str(Version(version).next_patch())
    else:
        raise ValueError("Invalid version part. Use 'major', 'minor', or 'patch'.")


def execute(command, capture_output=False):
    """
    The execute command will loop and keep on reading the stdout and check for the return code
    and displays the output in real time.
    """

    print(f"Running shell command: '{command}'")

    if capture_output:
        return subprocess.check_output(shlex.split(command))

    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)

    while True:
        output = process.stdout.readline()

        if output == b'' and process.poll() is not None:
            break
        if output:
            print(output.strip())

    return process.returncode


def update_version_in_setup_py(new_version_part) -> str:
    if execute(CHECK_GIT_TREE_CLEAN_CMD) != 0:
        print("ERROR: git tree not clean, aborting")
        sys.exit(1)

    # Read the setup.py file
    with open(f'{SETUP}', 'r') as file:
        setup_lines = file.readlines()

    old_version = None
    new_version = None

    # Find and update the version line
    version_pattern = r"__tag__\s*=\s*'(.*)'"
    updated_lines = []
    for line in setup_lines:
        match = re.match(version_pattern, line)
        if match:
            old_version = match.group(1)
            new_version = increase_version(old_version, new_version_part)

            print(f'Current version: {old_version}')
            new_version_confirm = input(f"Next version: {new_version}. Enter to confirm, or prompt the new version: ")

            if new_version_confirm != '':
                Version(new_version_confirm)  # check
                new_version = new_version_confirm

            updated_line = line.replace(old_version, new_version)
            updated_lines.append(updated_line)
        else:
            updated_lines.append(line)

    if new_version is not None and old_version is not None:
        execute(f'{GIT} tag {old_version}')

        # Write the updated content back to the setup.py file
        with open(f'{SETUP}', 'w') as file:
            file.writelines(updated_lines)

        execute(f'{GIT} add {SETUP}')

        if execute(f'{GIT} commit -m "prepared next version: {new_version}"') != 0:
            with open(f'{SETUP}', 'w') as file:
                file.writelines(setup_lines)

            execute(f'{GIT} tag --delete {old_version}')

            print('Rollbacked tag creation')
    else:
        print(f'WARNING: no tag detected in {SETUP}, aborting')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='release', description='Utility tool for releasing python library versions')

    parser.add_argument('part', choices=['major', 'minor', 'patch'], type=str, help='Version part update')
    parser.add_argument('--git', required=False, default='/usr/bin/git', help='Path to git executable')
    parser.add_argument('--bash', required=False, default='/bin/bash', help='Path to bash executable')
    parser.add_argument('--setup', required=False, default='setup.py', help='Path to setup.py file')

    args = parser.parse_args()

    print(args)

    version_part: str = args.part
    GIT = args.git
    BASH = args.bash
    SETUP = args.setup

    update_version_in_setup_py(version_part)
